import { IonCard, IonCardHeader, IonCardTitle, IonCardSubtitle, IonCardContent, IonButton, useIonAlert } from '@ionic/react';
import './ExploreContainer.css';
import { useEffect, useState } from 'react';
import axios from 'axios';

interface ContainerProps {
  name: string;
}

const ExploreContainer: React.FC<ContainerProps> = ({ name }) => {

  const [users, setUsers] = useState([]);
  const [userErros, setUserErrors] = useState([]);

  function getUsers() {
    var url = `http://localhost:8080/findCompteNonActive`;
    axios.get(url).then((response) => {
      setUsers(response.data);
    }
    ).catch((e) => {
      console.log("Error : ", e);
      setUserErrors(e);
    }
    )
  }

  useEffect(() => {
    getUsers()
  }, [])

  // activate user
  const [user, setUser] = useState([]);
  function getUser(id: Int16Array) {
    var url = `http://localhost:8080/utilisateur/${id}`;
    axios.get(url).then((response) => {
      setUser(response.data);
    }
    ).catch((e) => {
      console.log("Error : ", e);
    }
    )
  }

  const [presentAlert] = useIonAlert();
  function activateUser(id: Int8Array) {
    getUser(id);
    const message = `Activate ${user.nom} ${user.prenom} account?`;
    presentAlert({
      header: 'ACTIVATE ACCOUNT',
      message: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'OK',
          role: 'confirm',
        },
      ],
    })
    setUser([])
  }

  return (
    <div>

      {
        users.map((user, index) =>
          <>
            <IonCard>
              <img alt="Silhouette of mountains" src="https://ionicframework.com/docs/img/demos/card-media.png" />
              <IonCardHeader>
                <IonCardTitle>{user.nom} {user.prenom}</IonCardTitle>
                <IonCardSubtitle>{user.email}</IonCardSubtitle>
              </IonCardHeader>

              <IonCardContent>Here's a small text description for the card content. Nothing more, nothing less.</IonCardContent>

              <IonButton fill="outline" color={'success'} onClick={() => activateUser(user.id)} >ACTIVATE</IonButton>
            </IonCard>
          </>
        )
      }


    </div>
  );
};

export default ExploreContainer;
