import { IonCard, IonCardHeader, IonCardTitle, IonCardSubtitle, IonCardContent, IonButton, useIonAlert } from '@ionic/react';
import './ExploreContainer.css';
import { useEffect, useState } from 'react';
import axios from 'axios';

interface ContainerProps {
  name: string;
}

const ExploreContainer: React.FC<ContainerProps> = ({ name }) => {


  return (
    <div>

      <IonCard>
        <img alt="Silhouette of mountains" src="https://ionicframework.com/docs/img/demos/card-media.png" />
        <IonCardHeader>
          <IonCardTitle>HELLO</IonCardTitle>
          <IonCardSubtitle>test@gmail.com</IonCardSubtitle>
        </IonCardHeader>

        <IonCardContent>Here's a small text description for the card content. Nothing more, nothing less.</IonCardContent>

        <IonButton fill="outline" color={'success'}>ACTIVATE</IonButton>
      </IonCard>

    </div>
  );
};

export default ExploreContainer;
